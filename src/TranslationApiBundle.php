<?php

declare(strict_types=1);

namespace Dexodus\TranslationApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TranslationApiBundle extends Bundle
{
}

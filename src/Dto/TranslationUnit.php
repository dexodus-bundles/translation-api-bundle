<?php

declare(strict_types=1);

namespace Dexodus\TranslationApiBundle\Dto;

class TranslationUnit
{
    public string $key;
    public string $value;
}
